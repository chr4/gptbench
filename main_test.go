package main

import (
	"reflect"
	"testing"
)

func TestSortByOrder(t *testing.T) {
	tests := []struct {
		name string
		a    []string
		want []string
	}{
		{
			name: "example test",
			a:    []string{"D", "GL", "PH", "M", "CH"},
			want: []string{"M", "D", "GL", "CH", "PH"},
		},
		// Add more test cases as needed
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := sortByOrder(tt.a)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("got %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkSortByOrder(b *testing.B) {
	a := []string{"D", "GL", "PH", "M", "CH"}
	for n := 0; n < b.N; n++ {
		sortByOrder(a)
	}
}

func TestSortByOrderGPT4(t *testing.T) {
	tests := []struct {
		name string
		a    []string
		want []string
	}{
		{
			name: "example test",
			a:    []string{"D", "GL", "PH", "M", "CH"},
			want: []string{"M", "D", "GL", "CH", "PH"},
		},
		// Add more test cases as needed
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := sortByOrderGPT4(tt.a)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("got %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkSortByOrderGPT4(b *testing.B) {
	a := []string{"D", "GL", "PH", "M", "CH"}
	for n := 0; n < b.N; n++ {
		sortByOrderGPT4(a)
	}
}

func TestSortByOrderGPT4_2(t *testing.T) {
	tests := []struct {
		name string
		a    []string
		want []string
	}{
		{
			name: "example test",
			a:    []string{"D", "GL", "PH", "M", "CH"},
			want: []string{"M", "D", "GL", "CH", "PH"},
		},
		// Add more test cases as needed
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			sortByOrderGPT4_2(tt.a)
			if !reflect.DeepEqual(tt.a, tt.want) {
				t.Errorf("got %v, want %v", tt.a, tt.want)
			}
		})
	}
}

func BenchmarkSortByOrderGPT4_2(b *testing.B) {
	a := []string{"D", "GL", "PH", "M", "CH"}
	for n := 0; n < b.N; n++ {
		sortByOrderGPT4_2(a)
	}
}

func TestSortByOrderGPT3_2(t *testing.T) {
	tests := []struct {
		name string
		a    []string
		want []string
	}{
		{
			name: "example test",
			a:    []string{"D", "GL", "PH", "M", "CH"},
			want: []string{"M", "D", "GL", "CH", "PH"},
		},
		// Add more test cases as needed
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := sortByOrderGPT3_2(tt.a)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("got %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkSortByOrderGPT3_2(b *testing.B) {
	a := []string{"D", "GL", "PH", "M", "CH"}
	for n := 0; n < b.N; n++ {
		sortByOrderGPT4_2(a)
	}
}
