package main

import (
	"sort"
)

var sortOrder = []string{"M", "D", "E", "GL", "CH", "BIO", "PH", "SPO", "WPU"}

// sortByOrder sorts array a according to the order defined in array b.
func sortByOrderGPT4_2(a []string) {
	// Create a map to store the order of elements as per array b
	orderMap := make(map[string]int, len(sortOrder))
	for i, val := range sortOrder {
		orderMap[val] = i
	}

	// Use sort.Slice to sort array a according to orderMap
	sort.Slice(a, func(i, j int) bool {
		// Set a high default index for items not found in orderMap
		// This ensures they are placed at the end of the sorted slice
		const defaultIndex = int(^uint(0) >> 1) // Max int value
		indexI, okI := orderMap[a[i]]
		if !okI {
			indexI = defaultIndex
		}
		indexJ, okJ := orderMap[a[j]]
		if !okJ {
			indexJ = defaultIndex
		}
		return indexI < indexJ
	})
}

func sortByOrderGPT4(a []string) []string {
	orderMap := make(map[string]int)
	for i, val := range sortOrder {
		orderMap[val] = i
	}

	// Define a custom sort function using the order defined in b
	sort.SliceStable(a, func(i, j int) bool {
		// Use the positions in orderMap to decide the order
		// If an element is not found in orderMap, it will get the default int value 0, which is fine for unknown elements
		return orderMap[a[i]] < orderMap[a[j]]
	})

	return a
}

func sortByOrder(a []string) []string {
	sort.Slice(a, func(i, j int) bool {
		indexI := indexOf(sortOrder, a[i])
		indexJ := indexOf(sortOrder, a[j])
		return indexI < indexJ
	})

	return a
}

func indexOf(arr []string, element string) int {
	for i, value := range arr {
		if value == element {
			return i
		}
	}
	return -1
}

func sortByOrderGPT3_2(a []string) []string {
	indexMap := make(map[string]int, len(sortOrder))
	for i, v := range sortOrder {
		indexMap[v] = i
	}

	sort.Slice(a, func(i, j int) bool {
		return indexMap[a[i]] < indexMap[a[j]]
	})

	return a
}
